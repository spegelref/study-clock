package se.zxlisys.mld.studyclock.core;

import org.json.JSONArray;
import org.json.JSONObject;

public class CourseJSONFileExporter {
	private Course course;
	
	public CourseJSONFileExporter() {
		this(null);
	}
	
	public CourseJSONFileExporter(Course course) {
		this.course = course;
	}
	
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String exportJson() {
		JSONObject object = courseObject(course);
		
		return object.toString();
	}
	
	protected JSONObject courseObject(Course course) {
		JSONObject options = new JSONObject();

		options.put(Options.WEEKS.getName(), course.getWeeks());
		options.put(Options.HOURS_PER_COURSE.getName(),
				course.getHoursPerWeek());
		options.put(Options.CURRENT_WEEK.getName(), course.getCurrentWeek());

		JSONArray sessionOptions = new JSONArray();
		for (StudySession session : course.previousSessions()) {
			JSONObject obj = sessionObject(session);
			
			sessionOptions.put(obj);
		}
		
		options.put(Options.SESSIONS.getName(), sessionOptions);
		
		return options;
	}
	
	protected JSONObject sessionObject(StudySession session) {
		JSONObject options = new JSONObject();
		
		options.put(Options.SESSION_ID.getName(), session.getId());
		
		if (session.getStart() != null) {
			options.put(Options.SESSION_START.getName(), 
					session.getStart().getTime());
		}
		
		if (session.getStop() != null) {
			options.put(Options.SESSION_STOP.getName(), 
					session.getStop().getTime());
		}
		
		options.put(Options.SESSION_WEEK.getName(), session.getWeekNumber());
		
		return options;
	}
}

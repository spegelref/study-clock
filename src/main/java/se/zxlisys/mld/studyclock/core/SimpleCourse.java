package se.zxlisys.mld.studyclock.core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SimpleCourse implements Course {
	private int hours;
	private int weeks;
	
	private int currentWeek;
	
	private long sessionId;
	private StudySession currentSession;
	private List<StudySession> previousSessions;
	
	public SimpleCourse() {
		hours = 0;
		weeks = 1;
		
		currentWeek = 0;

		sessionId = 0;
		currentSession = null;
		previousSessions = new ArrayList<StudySession>();
	}
	
	@Override
	public int getHoursPerWeek() {
		return hours;
	}

	@Override
	public void setHoursPerWeek(int hours) {
		this.hours = hours;
	}

	@Override
	public int getWeeks() {
		return weeks;
	}

	@Override
	public void setWeeks(int weeks) {
		this.weeks = weeks;
	}

	@Override
	public int getCurrentWeek() {
		return currentWeek;
	}

	@Override
	public void setCurrentWeek(int week) {
		if (week >= weeks) {
			throw new IllegalArgumentException(
					"Current week may not be larger or equal to weeks.");
		}
		this.currentWeek = week;
	}

	@Override
	public void startSession() {
		if (currentSession != null) {
			throw new IllegalStateException("Session already started!");
		}
		
		currentSession = new StudySession(currentWeek);
	}

	@Override
	public void stopSession() {
		if (currentSession == null) {
			throw new IllegalStateException("Session is not started");
		}
		
		StudySession finishedSession = new StudySession(currentSession);
		finishedSession.setId(sessionId);
		
		previousSessions.add(finishedSession);
		
		sessionId += 1;
		currentSession = null;
	}

	@Override
	public StudySession currentSession() {
		return currentSession;
	}

	@Override
	public List<StudySession> previousSessions() {
		return previousSessions;
	}

	@Override
	public void modifySession(StudySession session, 
			StudySession modifiedSession) {
		if (session.getId() != modifiedSession.getId()) {
			throw new IllegalArgumentException("Cannot change id of session");
		}

		int index = previousSessions.indexOf(session);
		previousSessions.set(index, modifiedSession);
	}

	@Override
	public long timeLeft() {
		return timeLeft(currentWeek);
	}

	@Override
	public long timeLeft(int week) {
		long left = TimeUnit.MILLISECONDS.convert(hours, TimeUnit.HOURS);		
		
		// Subtract previous sessions time.
		List<StudySession> sessions = findSessionsForWeek(week);
		for (StudySession session : sessions) {
			left -= session.getInterval(); 
		}
		
		// Subtract current sessions time if any.
		if(currentSession != null &&
				week == currentSession.getWeekNumber()) {
			left -= currentSession.getInterval();
		}
		
		// Don't return a negative time.
		return left < 0 ? 0 : left;
	}
	
	private List<StudySession> findSessionsForWeek(int week) {
		List<StudySession> sessionsForWeek = new ArrayList<StudySession>();
		
		for (StudySession s : previousSessions) {
			if (week == s.getWeekNumber()) {
				sessionsForWeek.add(s);
			}
		}
		
		return sessionsForWeek;
	}
}

package se.zxlisys.mld.studyclock.core;

import java.util.Date;

public class StudySession {
	private long id;
	
	private Date start;
	private Date stop;
	
	private int weekNumber;

	public StudySession() {
		this(-1);
	}
	
	public StudySession(int weekNumber) {
		this(weekNumber, null);
	}
	
	public StudySession(int weekNumber, Date start) {
		this(weekNumber, start, null);
	}
	
	public StudySession(int weekNumber, Date start, Date stop) {
		super();
		
		this.start = start;
		this.stop = stop;
		
		this.weekNumber = weekNumber;
	}
	
	// Copy Constructor
	public StudySession(StudySession session) {
		this();

		this.start = session.start != null 
				? new Date(session.start.getTime()) : null;
		this.stop = session.stop != null 
				? new Date(session.stop.getTime()) : null;
				
		this.id = session.id;
		this.weekNumber = session.weekNumber;
	}

	// Getters and setters.
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getStop() {
		return stop;
	}

	public void setStop(Date stop) {
		this.stop = stop;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}
	
	// Public methods.

	public long getInterval() {
		if (start == null || stop == null) {
			return -1;
		}
				
		return stop.getTime() - start.getTime();
	}

	// Method overrides.
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		result = prime * result + ((stop == null) ? 0 : stop.hashCode());
		result = prime * result + weekNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudySession other = (StudySession) obj;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		if (stop == null) {
			if (other.stop != null)
				return false;
		} else if (!stop.equals(other.stop))
			return false;
		if (weekNumber != other.weekNumber)
			return false;
		return true;
	}
}

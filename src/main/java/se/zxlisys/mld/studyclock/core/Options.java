package se.zxlisys.mld.studyclock.core;

public enum Options {
	NAME("name"),
	HOURS_PER_COURSE("hours"),
	WEEKS("weeks"),
	CURRENT_WEEK("current"),
	
	SESSIONS("sessions"),
	
	SESSION_ID("session_id"),
	SESSION_WEEK("session_week"),
	SESSION_START("start"),
	SESSION_STOP("stop");
	
	private String name;
	
	private Options(String name) {
		this.name = name;
	}
	
	public String getName() {
		return toString();
	}
	
	@Override
	public String toString() {
		return name;
	}
}

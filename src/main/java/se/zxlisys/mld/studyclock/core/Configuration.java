package se.zxlisys.mld.studyclock.core;

public class Configuration {
	private int hours;
	private int weeks;
	private int current;
	
	public Configuration(int hours, int weeks, int current) {
		this.hours = hours;
		this.weeks = weeks;
		this.current = current;
	}
	
	public int getHours() {
		return hours;
	}
	
	public int getWeeks() {
		return weeks;
	}
	
	public int getCurrentWeek() {
		return current;
	}
}

package se.zxlisys.mld.studyclock.core;

import org.json.JSONObject;

public class ConfigurationJSONFileImporter {
	private String json;
	
	public ConfigurationJSONFileImporter() {
		this(null);
	}
	 
	public ConfigurationJSONFileImporter(String json) {
		 this.json = json;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
	
	public Configuration importJson() {
		int hours;
		int weeks;
		int current;
		
		JSONObject obj = parseJsonString(json);
		
		hours = obj.getInt(Options.HOURS_PER_COURSE.getName());
		weeks = obj.getInt(Options.WEEKS.getName());
		current = obj.getInt(Options.CURRENT_WEEK.getName());
		
		return new Configuration(hours, weeks, current);
	}
	
	private JSONObject parseJsonString(String jsonString) {
		JSONObject object = new JSONObject(jsonString);
		
		return object;
	}
}

package se.zxlisys.mld.studyclock.core;

import java.util.List;

public interface Course {
	int getHoursPerWeek();
	void setHoursPerWeek(int hours);

	int getWeeks();
	void setWeeks(int weeks);
	
	int getCurrentWeek();
	void setCurrentWeek(int week);
	
	void startSession();
	void stopSession();

	StudySession currentSession();
	
	List<StudySession> previousSessions();
	void modifySession(StudySession session, StudySession modifiedSession);
	
	long timeLeft();
	long timeLeft(int week);
}

package se.zxlisys.mld.studyclock.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandler {
	public static Configuration configuration() {
		File confFile = configurationFile();
		
		// If the configuration file don't exists. Return default conf.
		if (!confFile.exists()) {
			return new Configuration(40, 7, 0);
		}
	
		String json = "{}";
		try {
			json = readFile(confFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ConfigurationJSONFileImporter importer = 
				new ConfigurationJSONFileImporter(json);
		
		return importer.importJson();
	}
	
	public static Course load() {
		String json = "{}";
		try {
			json = readFile(saveFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		CourseJSONFileImporter importer = new CourseJSONFileImporter(json);
		
		return importer.importJson();
	}
	
	public static void save(Course course) {
		CourseJSONFileExporter exporter = new CourseJSONFileExporter(course);
		
		try {
			writeFile(saveFile(), exporter.exportJson());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static File saveFile() {
		File f = new File("./save.txt");
		
		return f;
	}
	
	public static File configurationFile() {
		File f = new File("./conf.txt");
		
		return f;
	}
	
	private static void writeFile(File file, String str) throws IOException {		
		FileWriter w = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(w);
		
		bw.write(str);
		bw.close();
	}
	
	private static String readFile(File file) throws IOException {
		FileReader r = new FileReader(file);
		BufferedReader br = new BufferedReader(r);
		
		StringBuilder output = new StringBuilder();
		String content = "";
		while ((content = br.readLine()) != null) {
			output.append(content);
		}
		
		br.close();
		
		return output.toString();
	}
}

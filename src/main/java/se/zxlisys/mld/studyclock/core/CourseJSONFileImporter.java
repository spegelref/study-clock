package se.zxlisys.mld.studyclock.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class CourseJSONFileImporter {
	private String json;
	
	public CourseJSONFileImporter() {
		this(null);
	}
	
	public CourseJSONFileImporter(String json) {
		this.json = json;
	}
	
	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public Course importJson() {
		Course c = new SimpleCourse();

		JSONObject obj = parseJsonString(json);
		
		c.setHoursPerWeek(obj.getInt(Options.HOURS_PER_COURSE.getName()));
		c.setWeeks(obj.getInt(Options.WEEKS.getName()));
		c.setCurrentWeek(obj.getInt(Options.CURRENT_WEEK.getName()));
		
		JSONArray array = obj.getJSONArray(Options.SESSIONS.getName());
		
		List<StudySession> sessions = parseStudyListArray(array);
		for (StudySession session : sessions) {
			c.startSession();
			
			StudySession cur = c.currentSession();
			
			cur.setId(session.getId());
			cur.setStart(session.getStart());
			cur.setStop(session.getStop());
			cur.setWeekNumber(session.getWeekNumber());

			c.stopSession();
		}
		
		return c;
	}
	
	private JSONObject parseJsonString(String jsonString) {
		JSONObject object = new JSONObject(jsonString);
		
		return object;
	}

	private List<StudySession> parseStudyListArray(JSONArray array) {
		List<StudySession> sessions = new ArrayList<StudySession>();
		
		for (int i = 0; i < array.length(); i++) {
			sessions.add(parseStudySession(array.getJSONObject(i)));
		}
		
		return sessions;
	}
	
	private StudySession parseStudySession(JSONObject sessionObject) {
		StudySession session = new StudySession();
		
		session.setId(sessionObject.getInt(Options.SESSION_ID.getName()));
		
		session.setStart(new Date(
				sessionObject.getLong(Options.SESSION_START.getName())));
		session.setStop(new Date(
				sessionObject.getLong(Options.SESSION_STOP.getName())));
	
		session.setWeekNumber(
				sessionObject.getInt(Options.SESSION_WEEK.getName()));
		
		return session;
	}
}

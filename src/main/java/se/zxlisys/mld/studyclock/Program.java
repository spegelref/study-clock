package se.zxlisys.mld.studyclock;

import se.zxlisys.mld.studyclock.core.Configuration;
import se.zxlisys.mld.studyclock.core.FileHandler;
import se.zxlisys.mld.studyclock.core.SimpleCourse;
import se.zxlisys.mld.studyclock.core.Course;
import se.zxlisys.mld.studyclock.view.MainTimerFrame;

public class Program {
	public static void main(String[] args) {
		Configuration conf = FileHandler.configuration();
		
		Course studyTimer = new SimpleCourse();
		studyTimer.setHoursPerWeek(conf.getHours());
		studyTimer.setWeeks(conf.getWeeks());
		studyTimer.setCurrentWeek(conf.getCurrentWeek());
		
		MainTimerFrame frame = new MainTimerFrame(studyTimer);
		frame.setVisible(true);
	}
}

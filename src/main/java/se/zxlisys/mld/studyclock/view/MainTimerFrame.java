package se.zxlisys.mld.studyclock.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;

import se.zxlisys.mld.studyclock.core.StudySession;
import se.zxlisys.mld.studyclock.core.Course;

public class MainTimerFrame extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private Course course;
	
	private long latestTimeValue;
	private long startTimeValue;
	
	// Components.
	private Timer timer;
	
	private JPanel contentPane;

	private JLabel currentTime;
	private JButton startButton;
	private JButton stopButton;
	
	public MainTimerFrame(Course course) {
		this.course = course;
		
		initialize();
	}
	
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	private void initialize() {
		timer = new Timer(127, this);

		contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		
		// Create timer box.
		Box timerBox = new Box(BoxLayout.LINE_AXIS);
		
		currentTime = new JLabel(formatTimerString(course.timeLeft()));
		currentTime.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
		timerBox.add(currentTime);
		
		// Create button box.
		Box buttonBox = new Box(BoxLayout.LINE_AXIS);
		
		startButton = new JButton("Start");
		startButton.addActionListener(this);
		
		stopButton = new JButton("Stop");
		stopButton.addActionListener(this);
		
		buttonBox.add(startButton);
		buttonBox.add(stopButton);
		
		// Add to content pane
		contentPane.add(timerBox);
		contentPane.add(buttonBox);
		
		// Configure frame
		setContentPane(contentPane);
		setJMenuBar(new MainMenu(this));
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Date now = new Date();

		if (e.getSource() == timer) {
			long time = latestTimeValue - (now.getTime() - startTimeValue);
			
			currentTime.setText(formatTimerString(time));
		}
		
		if (e.getSource() == startButton) {
			latestTimeValue = course.timeLeft();
			startTimeValue = now.getTime();
			
			course.startSession();
			course.currentSession().setStart(now);
			
			timer.start();
		} else if (e.getSource() == stopButton) {
			StudySession session = course.currentSession();
			if (session != null) {
				session.setStop(now);
			}
			
			course.stopSession();				
			timer.stop();
		}
	}
	
	private String formatTimerString(long milliseconds) {
		long seconds = milliseconds / 1000 % 60;
		long minutes =  milliseconds / (60 * 1000) % 60;
		long hours = milliseconds / (60 * 60 * 1000);
		
		return String.format("%02dh %02dm %02ds", hours, minutes, seconds);
	}
	
	//TODO: Change to property change listener.
	public void updateTimerLabel() {
		currentTime.setText(formatTimerString(course.timeLeft()));
	}
}

package se.zxlisys.mld.studyclock.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import se.zxlisys.mld.studyclock.core.FileHandler;

public class MainMenu extends JMenuBar implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	// Instance
	private MainTimerFrame ownerWindow;
	
	// File menu.
	private JMenu file;
	private JMenuItem fileImport;
	private JMenuItem fileExport;
	private JMenuItem filePrint;
	private JMenuItem fileQuit;
	
	// Edit menu.
	private JMenu edit;
	private JMenuItem editPreferences;
	
	// Session menu.
	private JMenu session;
	private JMenuItem sessionNew; 
	private JMenuItem sessionClose;
	private JMenuItem sessionList;
	
	// Tools menu.
	private JMenu tools;
	private JMenuItem toolsPreferences;
	
	// Window menu.
	private JMenu window;
	private JMenuItem windowMinimize;
	private JMenuItem windowZoom; 
	private JMenuItem windowBringAllToFront;
	
	// Help menu
	private JMenu help;
	private JMenuItem helpAbout;
	private JMenuItem helpManual;
	private JMenuItem helpWebsite;
	
	public MainMenu(MainTimerFrame ownerWindow) {
		this.ownerWindow = ownerWindow;
		
		initialize();
	}
	
	private void initialize() {
		// Create "File" menu.
		file = new JMenu("File");
		fileImport = new JMenuItem("Import");
		fileImport.addActionListener(this);
		fileExport = new JMenuItem("Export");
		fileExport.addActionListener(this);
		filePrint = new JMenuItem("Print");
		fileQuit = new JMenuItem("Quit");
		
		file.add(fileImport);
		file.add(fileExport);
		file.add(new JSeparator());
		file.add(filePrint);
		file.add(new JSeparator());
		file.add(fileQuit);

		// Create "Edit" menu.
		edit = new JMenu("Edit");
		editPreferences = new JMenuItem("Preferences");
		
		edit.add(editPreferences);
		
		// Create "Session" menu.
		session = new JMenu("Session");
		sessionNew = new JMenuItem("New");
		sessionClose = new JMenuItem("Close");
		sessionList = new JMenuItem("List");
		
		session.add(sessionNew);
		session.add(sessionClose);
		session.add(new JSeparator());
		session.add(sessionList);
		
		// Create "Tools" menu.
		tools = new JMenu("Tools");
		toolsPreferences = new JMenuItem("Preferences");
		
		tools.add(toolsPreferences);
		
		// Create "Window" menu.
		window = new JMenu("Window");
		windowMinimize = new JMenuItem("Minimize");
		windowZoom = new JMenuItem("Zoom");
		windowBringAllToFront = new JMenuItem("Bring All to Front");

		window.add(windowMinimize);
		window.add(windowZoom);
		window.add(new JSeparator());
		window.add(windowBringAllToFront);
		
		// Create "Help" menu.
		help = new JMenu("Help");
		helpAbout = new JMenuItem("About");
		helpManual = new JMenuItem("Manual");
		helpWebsite = new JMenuItem("Website");
		
		help.add(helpAbout);
		help.add(new JSeparator());
		help.add(helpManual);
		help.add(helpWebsite);
		
		// Add menus to the bar.
		add(file);
		add(edit);
		add(session);
		add(tools);
		add(window);
		add(help);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (fileImport == e.getSource()) {
			ownerWindow.setCourse(FileHandler.load());
			ownerWindow.updateTimerLabel();
		}
		else if (fileExport == e.getSource()) {
			FileHandler.save(ownerWindow.getCourse());
		}
	}
}

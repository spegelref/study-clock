package se.zxlisys.mld.studyclock.core;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;

public class StudySessionTest {
	
	@Test
	public void testConstructor() {
		Calendar cal = new GregorianCalendar(); 
		
		int week = 10;
		
		Date d1 = cal.getTime();
		cal.add(Calendar.HOUR, 1);
		Date d2 = cal.getTime();
		cal.add(Calendar.HOUR, 1);
		Date d3 = cal.getTime();
		
		// Test argument less constructor.
		StudySession s1 = new StudySession();
		assertEquals(s1.getWeekNumber(), -1);
		assertNull(s1.getStart());
		assertNull(s1.getStop());
		
		// Test single argument constructor (Setting only week).
		StudySession s2 = new StudySession(week);
		assertEquals(s2.getWeekNumber(), week);
		assertNull(s2.getStart());
		assertNull(s2.getStop());
		
		// Test two argument constructor (Setting week and only start date).
		StudySession s3 = new StudySession(week, d1);
		assertEquals(s3.getWeekNumber(), week);
		assertNotNull(s3.getStart());
		assertNull(s3.getStop());
		assertEquals(d1, s3.getStart());
		
		// Test three argument constructor (Setting week, start and stop).
		StudySession s4 = new StudySession(week, d2, d3);
		assertEquals(s3.getWeekNumber(), week);
		assertNotNull(s4.getStart());
		assertNotNull(s4.getStop());
		assertEquals(s4.getStart(), d2);
		assertEquals(s4.getStop(), d3);
	}
	
	@Test
	public void testCopyConstructor() {
		int week = 4;
		
		Calendar cal = new GregorianCalendar();
		Date d1 = cal.getTime(); cal.add(Calendar.HOUR, 2);
		Date d2 = cal.getTime();
		
		StudySession s1 = new StudySession(week, d1, d2);
		StudySession s2 = new StudySession(s1);
		
		assertTrue("Sessions should not be the same", s1 != s2);
		
		assertEquals("Sessions should be equal", s1, s2);
		assertEquals("ID should match", s1.getId(), s2.getId());
		
		StudySession s3 = new StudySession();
		StudySession s4 = new StudySession(s3);
		assertNull(s4.getStart());
		assertNull(s4.getStop());
	}
	
	@Test
	public void testGetSetId() {
		long id = 1;
		
		StudySession session = new StudySession();
		session.setId(id);
		
		assertEquals(session.getId(), id);
	}
	
	@Test
	public void testGetSetStart() {
		Date d = new Date();

		StudySession session = new StudySession();
		session.setStart(d);
		
		assertEquals(session.getStart(), d);
	}
	
	@Test
	public void testGetSetStop() {
		Date d = new Date();

		StudySession session = new StudySession();
		session.setStop(d);
		
		assertEquals(session.getStop(), d);
	}
	
	@Test
	public void testGetSetWeekNumber() {
		int week = 0;
		
		StudySession session = new StudySession();
		session.setWeekNumber(week);
		
		assertEquals(session.getWeekNumber(), week);
	}
	
	@Test
	public void testGetInterval() {
		int seconds = 15;
		int millisPerSecond = 1000;
		
		Calendar cal = new GregorianCalendar(); 
		
		Date start1 = cal.getTime();
		Date start2 = cal.getTime();
		
		cal.add(Calendar.SECOND, seconds);
		Date stop = cal.getTime();
		
		// Interval should be 15s long.
		StudySession session1 = new StudySession(0, start1, stop);
		assertEquals(session1.getInterval(), seconds * millisPerSecond);
		
		// Interval should be 0s long.
		StudySession session2 = new StudySession(0, start1, start2);
		assertEquals(session2.getInterval(), 0);
		
		// If either is null. Return -1
		StudySession session3 = new StudySession(0, start1);
		StudySession session4 = new StudySession();
		session4.setStop(stop);
		StudySession session5 = new StudySession();

		assertEquals(session3.getInterval(), -1);
		assertEquals(session4.getInterval(), -1);
		assertEquals(session5.getInterval(), -1);
	}
	
	@Test
	public void testEquals() {
		Calendar cal = new GregorianCalendar();
		Date d1 = cal.getTime(); cal.add(Calendar.HOUR, 1);
		Date d2 = cal.getTime(); cal.add(Calendar.HOUR, 1);
		Date d3 = cal.getTime();
		
		StudySession s1 = new StudySession(1, d1, d2);
		StudySession s2 = new StudySession(1, d1, d2);
		StudySession s3 = new StudySession(2, d2, d3);
		StudySession s4 = new StudySession(2, d2, d3);
		
		// Equals should ignore id.
		assertTrue(s1.equals(s2));
		s1.setId(1);
		s2.setId(2);
		assertTrue(s1.equals(s2));
		
		// Equals should care about week.
		s3.setId(3);
		s4.setId(3);
		assertTrue(s3.equals(s4));
		s4.setWeekNumber(3);
		assertFalse(s3.equals(s4));
		
		// Equals should care about dates.
		s1.setId(s3.getId());
		s1.setWeekNumber(s3.getWeekNumber());
		assertFalse(s1.equals(s3));
		
		// Only change start date.
		s1.setStart(s3.getStart());
		assertFalse(s1.equals(s3));
		
		// Change stop date.
		s1.setStop(s3.getStop());
		assertTrue(s1.equals(s3));
		
		s2.setId(s4.getId());
		s2.setWeekNumber(s4.getWeekNumber());
		assertFalse(s2.equals(s4));
		
		// Only change stop date.
		s2.setStop(s4.getStop());
		assertFalse(s2.equals(s4));
		
		// Change start date.
		s2.setStart(s4.getStart());
		assertTrue(s2.equals(s4));
	}
	
	@Test
	public void testHashCode() {
		Calendar cal = new GregorianCalendar();
		Date d1 = cal.getTime(); cal.add(Calendar.HOUR, 1);
		Date d2 = cal.getTime(); cal.add(Calendar.HOUR, 1);
		Date d3 = cal.getTime();
		
		StudySession s1 = new StudySession(1, d1, d2);
		StudySession s2 = new StudySession(1, d1, d2);
		StudySession s3 = new StudySession(2, d2, d3);
		StudySession s4 = new StudySession(2, d2, d3);

		assertTrue(s1.equals(s2) && s1.hashCode() == s2.hashCode());
		assertTrue(s3.equals(s4) && s3.hashCode() == s4.hashCode());
	}
}

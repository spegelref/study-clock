package se.zxlisys.mld.studyclock.core;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

public abstract class CourseTest {
	public abstract Course createInstance();
	
	public Course timer;
	
	public CourseTest() {
		this.timer = createInstance();
	}
	
	@Test
	public void testHoursPerWeek() {
		// Validate getter and setter for Hour per Weeks.
		int hours = 25;
		
		timer.setHoursPerWeek(hours);
		assertEquals(timer.getHoursPerWeek(), hours);
	}
	
	@Test
	public void testWeek() {
		// Validate getter and setter for weeks.
		int weeks = 7;
		
		timer.setWeeks(weeks);
		assertEquals(timer.getWeeks(), weeks);
	}
	
	@Test
	public void testCurrentWeek() {
		// Validate getter and setter for current week.
		int week = 0;
		
		timer.setWeeks(1);
		timer.setCurrentWeek(week);
		assertEquals(timer.getCurrentWeek(), week);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testWeekEqualsWeeksShouldThrow() {
		int weeks = 7;
		int week = 7;
		
		timer.setWeeks(weeks);
		// Should throw exception since week >= weeks
		timer.setCurrentWeek(week);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testWeekLargerThanWeeksShouldThrow() {
		int weeks = 7;
		int week = 8;
		
		timer.setWeeks(weeks);
		// Should throw exception since week > weeks
		timer.setCurrentWeek(week);
	}
	
	@Test
	public void testSessionCreation() {
		// Calling startSession should create a session.
		// Calling stopStop should finish it (current should be null again).
		assertNull(timer.currentSession());
		
		timer.startSession();
		assertNotNull(timer.currentSession());
		
		timer.stopSession();
		assertNull(timer.currentSession());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testStartingTwoShouldThrow() {
		timer.startSession();
		timer.startSession();
	}
	
	@Test(expected=IllegalStateException.class)
	public void testStoppingNonStartedShouldThrow() {
		timer.stopSession();
	}
	
	@Test
	public void testSessionsAreTheSame() {
		// Calling currentSession() within a start and stop should
		// return the same session.
		timer.startSession();
		StudySession s1 = timer.currentSession();
		StudySession s2 = timer.currentSession();
		timer.stopSession();
		
		assertTrue(s1 == s2);
	}
	
	@Test
	public void testSessionsAreNew() {
		// The different session given by current session should be
		// different, each start and stop.
		timer.startSession();
		StudySession s1 = timer.currentSession();
		timer.stopSession();
		
		timer.startSession();
		StudySession s2 = timer.currentSession();
		timer.stopSession();
		
		assertNotSame(s1, s2);
	}
	
	@Test
	public void testSessionCount() {
		// After stopping a sessions should not be put in the 
		// previousSessions() list.
		List<StudySession> sessions = timer.previousSessions();
		assertNotNull(sessions);
		assertEquals("Size should be zero", sessions.size(), 0);
		
		timer.startSession();
		timer.stopSession();
		
		sessions = timer.previousSessions();
		
		assertEquals("Size should be one", sessions.size(), 1);
		
		timer.startSession();
		timer.stopSession();
		
		sessions = timer.previousSessions();
		
		assertEquals("Size should be two", sessions.size(), 2);
	}
	
	@Test
	public void testSessionAddingCorrectSessions() {
		// the sessions in the previousSessions list should be the
		// current sessions after stop.
		timer.startSession();
		StudySession s1 = timer.currentSession();
		timer.stopSession();
		
		timer.startSession();
		StudySession s2 = timer.currentSession();
		timer.stopSession();
		
		List<StudySession> sessions = timer.previousSessions();
		assertTrue("Should contain first session", sessions.contains(s1));
		assertTrue("Should contain second session", sessions.contains(s2));
		assertFalse("Should not contain new session",
				sessions.contains(new StudySession()));
	}
	
	@Test
	public void testSessionModification() {
		// Modifying sessions should be possible.
		int second = 15;
		
		Calendar cal = new GregorianCalendar();
		Date d1 = cal.getTime();
		cal.add(Calendar.SECOND, second);
		Date d2 = cal.getTime();
		
		timer.startSession();
		StudySession s1 = timer.currentSession();
		timer.stopSession();
		
		StudySession s2 = new StudySession();
		s2.setId(s1.getId());
		s2.setWeekNumber(s1.getWeekNumber());
		
		s2.setStart(d1);
		s2.setStop(d2);
		
		timer.modifySession(s1, s2);
		
		List<StudySession> sessions = timer.previousSessions();
		assertTrue(sessions.contains(s2));
		assertFalse(sessions.contains(s1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testModifyIdException() {
		// Exceptions should be thrown if we try to change the id.
		timer.startSession();
		StudySession s1 = timer.currentSession();
		timer.stopSession();
		
		StudySession s2 = new StudySession();
		s2.setId(s1.getId() + 1);
		
		timer.modifySession(s1, s2);
	}
	
	@Test
	public void testTimeLeft() {
		int hours = 10;

		Calendar cal = new GregorianCalendar();
		Date d1 = cal.getTime(); cal.add(Calendar.HOUR, 1);
		Date d2 = cal.getTime(); cal.add(Calendar.HOUR, 1);
		Date d3 = cal.getTime(); cal.add(Calendar.HOUR, 8);
		Date d4 = cal.getTime();
		
		timer.setHoursPerWeek(hours);
		timer.setWeeks(1);
		timer.setCurrentWeek(0);
		
		// With no logged or active sessions return 10h.
		assertEquals(timer.timeLeft(), 
				TimeUnit.MILLISECONDS.convert(hours, TimeUnit.HOURS));
		
		timer.startSession();
		StudySession s1 = timer.currentSession();
		s1.setStart(d1);
		s1.setStop(d2);
		timer.stopSession();

		// Time between d1 and d2 is 1h, time left should return 9h.
		assertEquals(timer.timeLeft(), 
				TimeUnit.MILLISECONDS.convert(hours-1, TimeUnit.HOURS));

		timer.startSession();
		StudySession s2 = timer.currentSession();
		s2.setStart(d2);
		s2.setStop(d3);
		timer.stopSession();

		// Time between d2 and d3 is yet 1h (total 2h logged)
		// Should return 8h.
		assertEquals(timer.timeLeft(), 
				TimeUnit.MILLISECONDS.convert(hours-2, TimeUnit.HOURS));
		
		timer.startSession();
		StudySession s3 = timer.currentSession();
		s3.setStart(d3);
		s3.setStop(d4);
		timer.stopSession();

		// Time between d3 to d4 is 8h which (totals to 10h logged).
		// time left should be zero.
		assertEquals(timer.timeLeft(), 0);
	}
	
	@Test
	public void testActiveSession() {
		int hours = 10;

		Calendar cal = new GregorianCalendar();
		Date d1 = cal.getTime(); cal.add(Calendar.HOUR, 1);
		Date d2 = cal.getTime(); cal.add(Calendar.HOUR, 1);
		Date d3 = cal.getTime();
		
		timer.setHoursPerWeek(hours);
		timer.setWeeks(1);
		timer.setCurrentWeek(0);
		
		timer.startSession();
		StudySession s1 = timer.currentSession();
		s1.setStart(d1);
		s1.setStop(d2);
		
		// Active session is 1h, time left should be 9h.
		assertEquals(timer.timeLeft(),
				TimeUnit.MILLISECONDS.convert(hours-1, TimeUnit.HOURS));
		timer.stopSession();
		
		// Stopping a session shouldn't change time left.
		assertEquals(timer.timeLeft(), 
				TimeUnit.MILLISECONDS.convert(hours-1, TimeUnit.HOURS));
		
		
		// Start new active session.
		timer.startSession();
		StudySession s2 = timer.currentSession();
		s2.setStart(d2);
		s2.setStop(d3);

		// Active session is 1h, logged session is 1h, time left should be 8h.
		assertEquals(timer.timeLeft(), 
				TimeUnit.MILLISECONDS.convert(hours-2, TimeUnit.HOURS));
		timer.stopSession();

		// Stopping a session shouldn't change time left.
		assertEquals(timer.timeLeft(),
				TimeUnit.MILLISECONDS.convert(hours-2, TimeUnit.HOURS));
	}
	
	@Test
	public void testPreviousWeek() {
		int hours = 10;
		
		timer.setHoursPerWeek(hours);
		timer.setWeeks(2);
		timer.setCurrentWeek(0);
		
		Calendar cal = new GregorianCalendar();
		Date d1 = cal.getTime(); cal.add(Calendar.HOUR, 10);
		Date d2 = cal.getTime();
		
		// Start new active session.
		timer.startSession();
		StudySession s1 = timer.currentSession();
		s1.setWeekNumber(0);
		s1.setStart(d1);
		s1.setStop(d2);
		timer.stopSession();

		assertEquals(0, timer.timeLeft());
		assertEquals(0, timer.timeLeft(0));
		assertEquals(TimeUnit.MILLISECONDS.convert(hours, TimeUnit.HOURS),
				timer.timeLeft(1));
		
		timer.setCurrentWeek(1);
		
		timer.startSession();
		StudySession s2 = timer.currentSession();
		s2.setWeekNumber(1);
		s2.setStart(d1);
		s2.setStop(d2);
		timer.stopSession();
		
		assertEquals(0, timer.timeLeft());
		assertEquals(0, timer.timeLeft(0));
		assertEquals(0, timer.timeLeft(1));
	}
	
	@Test
	public void testStoppedSessionShouldnotModify() {
		int hours = 10;
		
		Calendar cal = new GregorianCalendar();
		Date d1 = cal.getTime(); cal.add(Calendar.HOUR, 2);
		Date d2 = cal.getTime(); cal.add(Calendar.HOUR, 2);
		Date d3 = cal.getTime();
		
		timer.setHoursPerWeek(hours);
		timer.setWeeks(1);
		timer.setCurrentWeek(0);
		
		timer.startSession();
		StudySession s = timer.currentSession();
		s.setStart(d1);
		s.setStop(d2);
		timer.stopSession();
		
		assertEquals(timer.timeLeft(), 
				TimeUnit.MILLISECONDS.convert(8, TimeUnit.HOURS));
		
		// Changing the stopped session should not modify the session.
		s.setStop(d3);
		assertEquals(timer.timeLeft(), 
				TimeUnit.MILLISECONDS.convert(8, TimeUnit.HOURS));
	}
}
